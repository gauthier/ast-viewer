module Client

open Elmish
open Elmish.React
open Fable.Core.JsInterop
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Shared
open Fulma
open Fulma.FontAwesome
open ReactEditor
open ReactJsonView
open Fable.PowerPack
open Fable.Import.Monaco.Editor
open Fable.Import.Monaco
open GraphView
open Fable.SimpleJson
open System

importSideEffects "./sass/style.sass"
module Result =
    let toOption = function
        | Ok x -> Some x
        | Error _ -> None

let memoizeBy (f: 'a -> 'b) (g: 'a -> 'c) =
    let cache = System.Collections.Generic.Dictionary<_, _>(HashIdentity.Structural)
    fun x ->
        let key = Some (g x)
        let (result, y) = cache.TryGetValue(key)
        if result then y
        else
            let z = f x
            cache.Add(key, z)
            z

let memoize f = memoizeBy f id
let memoize2 f = memoize (fun (x,y) -> f x y) |> fun f -> fun x y -> f (x,y)

module URI =
    let private compressToEncodedURIComponent(_x: string): string  = importMember "./js/util.js"
    let private decompressFromEncodedURIComponent(_x: string): string  = importMember "./js/util.js"
    let private getURIhash(): string  = importMember "./js/util.js"
    let private setURIhash(_x: string): unit  = importMember "./js/util.js"

    let parseQuery() =
        getURIhash().Split[|'&'|]
        |> Seq.choose (fun s ->
            match s.Split[|'='|] |> Seq.toList with
            | [key; value] -> Some (key, decompressFromEncodedURIComponent value)
            | _ -> None)
        |> Map.ofSeq

    let updateQuery m =
        m |> Map.toSeq |> Seq.map (fun (key, value) -> key + "=" + compressToEncodedURIComponent value) |> String.concat "&"
        |> setURIhash

// The Msg type defines what events/actions can occur while the application is running
// the state of the application changes *only* in reaction to these events
type GraphMsg =
| SetRoot of Shared.Node
| RootBack
| SetOptions of Shared.Graph.Options

type Msg =
| VersionFound of string
| SetSourceText of string
| DoParse
| DoTypeCheck
| Parsed of Dto
| TypeChecked of Dto
| Error of string
| ShowJsonViewer
| ShowEditor
| ShowRaw
| ShowGraph
| Graph of GraphMsg
| DefinesUpdated of string
| FileNameUpdated of string

module Server =

    open Fable.Remoting.Client

    /// A proxy you can use to talk to server directly
    let api : IModelApi =
      Remoting.createApi()
      |> Remoting.withRouteBuilder Route.builder
      #if !DEBUG
      |> Remoting.withBaseUrl Config.azureFunctionsUrl
      #endif
      |> Remoting.buildProxy<IModelApi>

let mutable editor : IStandaloneCodeEditor option = None

// defines the initial state and initial command (= side-effect) of the application
let init () : Model * Cmd<Msg> =
    let initialModel = Model.Default
    let cmd = Cmd.ofAsync Server.api.version () VersionFound (fun ex -> Error ex.Message)
    let query = URI.parseQuery()
    let code = query |> Map.tryFind "code" |> Option.defaultValue ""
    let filename = query |> Map.tryFind "filename" |> Option.defaultValue initialModel.FileName
    let defines = query |> Map.tryFind "defines" |> Option.defaultValue ""
    { initialModel with
        Source = code
        FileName = filename
        Defines = defines}, Cmd.batch [cmd; Cmd.ofMsg (DoParse)]

let private getDefines (model:Model) =
    model.Defines.Split([|' ';',';';'|], StringSplitOptions.RemoveEmptyEntries)

// The update function computes the next state of the application based on the current state and the incoming events/messages
// It can also run side-effects (encoded as commands) like calling the server via Http.
// these commands in turn, can dispatch messages to which the update function will react.
let update (msg : Msg) (currentModel : Model) : Model * Cmd<Msg> =
    match msg with
    | SetSourceText x ->
        let nextModel = { currentModel with Source = x }
        nextModel, Cmd.none
    | Parsed x ->
        let nextModel = { currentModel with IsLoading = false; Parsed = Ok (Some x) }
        nextModel, Cmd.none
    | TypeChecked x ->
        let nextModel = { currentModel with IsLoading = false; Parsed = Ok (Some x) }
        nextModel, Cmd.none
    | Error e ->
        let nextModel = { currentModel with IsLoading = false; Parsed = Result.Error e }
        nextModel, Cmd.none
    | DoParse ->
        URI.updateQuery (Map.ofSeq ["code", currentModel.Source
                                    "defines", currentModel.Defines
                                    "filename", currentModel.FileName])
        let response =
            Cmd.ofAsync
                (fun x -> Server.api.parse x)
                { SourceCode = currentModel.Source; Defines = getDefines currentModel; FileName = currentModel.FileName }
                (function |Ok x -> Parsed x | Result.Error e -> Error e)
                (fun e -> Error (e.Message + "\n" + e.StackTrace))
        { currentModel with IsLoading = true }, response
    | DoTypeCheck ->
        URI.updateQuery (Map.ofSeq ["code", currentModel.Source
                                    "defines", currentModel.Defines
                                    "filename", currentModel.FileName])
        let response =
            Cmd.ofAsync
                (fun x -> Server.api.typeCheck x)
                { SourceCode = currentModel.Source; Defines = getDefines currentModel; FileName = currentModel.FileName }
                (function |Ok x -> TypeChecked x | Result.Error e -> Error e)
                (fun e -> Error (e.Message + "\n" + e.StackTrace))
        { currentModel with IsLoading = true }, response
    | VersionFound version -> { currentModel with Version = version }, Cmd.none
    | ShowJsonViewer -> {currentModel with View = Shared.JsonViewer}, Cmd.none
    | ShowEditor -> {currentModel with View = Shared.Editor}, Cmd.none
    | ShowRaw -> {currentModel with View = Shared.Raw}, Cmd.none
    | ShowGraph ->
        {currentModel with View = Shared.Graph},
        Cmd.ofAsync (fun _ -> Async.Sleep 100) () (fun _ -> Graph <| SetOptions currentModel.Graph.Options) (fun _ -> Error "")
    | Graph (SetRoot node) -> {currentModel with Graph = { currentModel.Graph with RootsPath = node :: currentModel.Graph.RootsPath }}, Cmd.none
    | Graph RootBack ->
        { currentModel with Graph = { currentModel.Graph with RootsPath = currentModel.Graph.RootsPath |> function | [] -> [] | _::tl -> tl }}, Cmd.none
    | Graph (SetOptions opt) -> {currentModel with Graph = { currentModel.Graph with Options = opt }}, Cmd.none
    | DefinesUpdated defines -> { currentModel with Defines = defines }, Cmd.none
    | FileNameUpdated fileName -> { currentModel with FileName = fileName }, Cmd.none

let safeComponents =
    let astViewerlinks =
        span [ ]
           [
             a [ Href "https://gitlab.com/jindraivanek/ast-viewer" ] [ str "source code" ]
             str ", "
             a [ Href "https://gitlab.com/jindraivanek/ast-viewer/issues/new" ] [ str "create issue" ]
           ]

    [ p [ ]
        [ strong [] [ str "AST-viewer " ]
          astViewerlinks ]
 ]

let button disabled txt onClick =
    Button.button
        [ Button.IsFullWidth
          Button.Color IsPrimary
          Button.OnClick onClick
          Button.Disabled disabled ]
        [ str txt ]

let viewNavbar version =
        Navbar.navbar [ Navbar.IsFixedTop ]
                [ Navbar.Brand.div [ ]
                    [ Navbar.Item.a [ ]
                        [ strong [ ]
                            [ sprintf "F# AST viewer - FCS version v%s" version |> str ] ] ]
                  Navbar.End.div [ ]
                    [ Navbar.Item.div [ ]
                        [ Button.a [ Button.Props [ Href "https://gitlab.com/jindraivanek/ast-viewer" ]
                                     Button.Color IsWarning ]
                            [ Icon.faIcon [ ]
                                [ Fa.icon Fa.I.Gitlab ]
                              span [ ]
                                [ str "Gitlab" ] ] ] ] ]

let sourceAndFormatted (model: Model) dispatch sourceTooBig =

    let headers =
        Columns.columns [ Columns.IsGapless ; Columns.IsMultiline ; Columns.CustomClass "is-gapless" ]
                        [ Column.column [ Column.Width(Screen.All, Column.IsHalf) ]
                            [ Message.message [ Message.Props [Id "input-message"] ]
                                [ Message.body [ ]
                                    [ Text.div [ Modifiers [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ] ]
                                        [ str "Type or paste F# code" ] ] ] ]
                          Column.column [ Column.Width(Screen.All, Column.IsHalf) ]
                            [ Message.message [ Message.Props [Id "formatted-message"] ]
                                [ Message.body [ ]
                                    [ Text.div [ Modifiers [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ] ]
                                        [ str "F# AST" ] ] ] ] ]

    let button opts txt onClick =
        Button.button
            [ yield Button.IsFullWidth
              yield Button.OnClick onClick
              yield! opts]
            [ str txt ]

    let settings =
        Columns.columns [Columns.IsGapless] [
            Column.column [] [
                Field.div []
                    [ Control.div []
                          [ Input.text
                              [ Input.Placeholder "Compiler defines, separate with ';'"
                                Input.Value model.Defines
                                Input.OnChange(fun ev ->
                                    ev.Value
                                    |> DefinesUpdated
                                    |> dispatch) ] ] ] ]

            Column.column [] [
                Field.div []
                    [ Control.div []
                          [ Input.text
                              [ Input.Placeholder "Filename"
                                Input.Value model.FileName
                                Input.OnChange(fun ev ->
                                    ev.Value
                                    |> FileNameUpdated
                                    |> dispatch) ] ] ] ]
        ]



    let editorButtons =
        Columns.columns []
                    [ Column.column [ Column.Width(Screen.All, Column.Is3) ]
                        [button [Button.Color IsInfo] "Show JsonViewer" (fun _ -> dispatch (ShowJsonViewer))]
                      Column.column [ Column.Width(Screen.All, Column.Is3) ]
                        [button [Button.Color IsInfo] "Show editor" (fun _ -> dispatch (ShowEditor))]
                      Column.column [ Column.Width(Screen.All, Column.Is3) ]
                        [button [Button.Color IsInfo] "Show raw" (fun _ -> dispatch (ShowRaw))]
                      Column.column [ Column.Width(Screen.All, Column.Is3) ]
                        [button [Button.Color IsInfo] "Show graph" (fun _ -> dispatch (ShowGraph))]
                    ]

    let editors =
        Columns.columns [ Columns.IsGapless ; Columns.IsMultiline ; Columns.CustomClass "is-gapless" ]
            [
              Column.column [] [
                settings
                Editor.editor [ Editor.Language "fsharp"
                                Editor.IsReadOnly false
                                Editor.Value model.Source
                                Editor.GetEditor(fun n -> editor <- Some n )
                                Editor.OnChange (SetSourceText >> dispatch) ]
                (if sourceTooBig then Notification.notification [ Notification.Color IsDanger ] [str (sprintf "Source code size is limited to 10 kB.")] else div [] []) ]
              Column.column [] [
                  Control.div [Control.IsLoading model.IsLoading; Control.CustomClass "is-large";] [
                        yield editorButtons
                        yield
                            match model.Parsed with
                            | Ok (Some parsed) ->
                                match model.View with
                                | Shared.Raw ->
                                    Editor.editor [ Editor.Language "fsharp"
                                                    Editor.IsReadOnly true
                                                    Editor.Value parsed.String ]
                                | Shared.Editor ->
                                    Editor.editor [ Editor.Language "fsharp"
                                                    Editor.IsReadOnly true
                                                    Editor.Value parsed.NodeJson ]
                                | Shared.JsonViewer ->
                                    Control.div [Control.CustomClass "viewer"] [
                                        JsonViewer.viewer [ JsonViewer.Src (Fable.Import.JS.JSON.parse parsed.NodeJson)
                                                            JsonViewer.Name null
                                                            JsonViewer.DisplayDataTypes false
                                                            JsonViewer.DisplayObjectSize false
                                                            JsonViewer.IndentWidth 2
                                                            JsonViewer.OnLookup (fun (o) ->
                                                                editor |> Option.iter (fun editor ->
                                                                    let range = createEmpty<Fable.Import.Monaco.IRange>
                                                                    range.endColumn <- !!(o.value?EndCol) + 1
                                                                    range.endLineNumber <- !!(o.value?EndLine)
                                                                    range.startColumn <- !!(o.value?StartCol) + 1
                                                                    range.startLineNumber <- !!(o.value?StartLine)
                                                                    editor.setSelection(range)
                                                                    editor.revealRangeInCenter(range, ScrollType.Smooth)
                                                                )
                                                            )
                                                            JsonViewer.ShouldLookup(fun (o) ->o.key = "Range")
                                                            JsonViewer.ShouldCollapse (fun x -> x?name = "Range")]
                                    ]
                                | Shared.Graph ->
                                    let node = parsed.NodeJson |> Thoth.Json.Decode.Auto.fromString<Shared.Node> |> Result.toOption |> Option.get
                                    let rec propertiesToHtml inArray p =
                                        match p with
                                        | JNumber x -> sprintf "%f" x
                                        | JString x -> x
                                        | JBool x -> sprintf "%b" x
                                        | JNull -> "null"
                                        | JArray [JString k; v] when inArray ->
                                            propertiesToHtml false (JObject (Map.ofSeq[k,v]))
                                        | JArray xs -> xs |> Seq.map (propertiesToHtml true >> sprintf "<li>%s</li>") |> String.concat "" |> sprintf "<ul>%s</ul>"
                                        | JObject o ->
                                            o |> Map.filter (fun k _ -> not (k.ToLower().Contains "range")) |> Map.toSeq
                                            |> Seq.map (fun (k, v) -> sprintf "<li><b>%s: </b>%s</li>" k (propertiesToHtml false v))
                                            |> String.concat "" |> sprintf "<ul>%s</ul>"

                                    let simpleType (t: string) = t.Split([|'.'|]) |> Seq.last
                                    let rec buildTree isRoot n = {
                                        Label = simpleType n.Type
                                        Color = if isRoot then Some "lime" else None
                                        Tooltip =
                                            Thoth.Json.Encode.Auto.toString(0, n.Properties) |> SimpleJson.parse
                                            |> fun x -> JArray[JString (sprintf "<b><i>%s</i></b>" n.Type); x] |> propertiesToHtml false
                                        Childrens = List.map (buildTree false) n.Childs
                                        Original = n }
                                    let buildTree n = buildTree true n
                                    let limitTree = memoize2 <| fun maxDepth n ->
                                        let rec f x n =
                                            let limit = x >= maxDepth && n.Childrens.Length > 0
                                            { n with Childrens = if limit then [] else List.map (f (x+1)) n.Childrens
                                                     Color = if limit then Some "cyan" else None}
                                        f 0 n
                                    let rec treeSize n =
                                        1 + (n.Childrens |> List.sumBy treeSize)
                                    let treeSize = memoize <| fun n -> treeSize n
                                    let limitTreeByNodes = memoize2 <| fun maxNodes n ->
                                        let xs = Seq.initInfinite (fun i -> let t = limitTree i n in treeSize t, t)
                                        xs |> Seq.pairwise |> Seq.find (fun ((c1, _), (c2,_)) -> (c1 <= maxNodes && c2 > maxNodes) || c1=c2)
                                        |> fst |> snd
                                    let rec childsRange = memoize <| fun n ->
                                        match n.Range with
                                        | Some r -> Some r
                                        | None ->
                                        let rs = n.Childs |> Seq.choose childsRange
                                        if Seq.isEmpty rs then None else Some (rs |> Seq.reduce (fun r1 r2 -> {
                                            StartLine = min r1.StartLine r2.StartLine
                                            StartCol = min r1.StartCol r2.StartCol
                                            EndLine = max r1.EndLine r2.EndLine
                                            EndCol = max r1.EndCol r2.EndCol
                                            }))
                                    let onHover (n: Node) =
                                        //Fable.Import.Browser.console.log("hoverNode Event", n, editor)
                                        editor |> Option.iter (fun editor ->
                                            let range = createEmpty<Fable.Import.Monaco.IRange>
                                            n |> childsRange |> Option.iter (fun r ->
                                                range.endColumn <- r.EndCol + 1
                                                range.endLineNumber <- r.EndLine
                                                range.startColumn <- r.StartCol + 1
                                                range.startLineNumber <- r.StartLine)
                                            editor.setSelection(range)
                                            editor.revealRangeInCenter(range, ScrollType.Smooth)
                                        )
                                    let onSelect (n: Node) = dispatch (Graph <| GraphMsg.SetRoot n)

                                    let root = model.Graph.RootsPath |> List.tryHead |> Option.defaultValue node
                                    let graphButtons =
                                        Columns.columns [ ]
                                            [ Column.column [ Column.Width(Screen.All, Column.Is3) ]
                                                [if not model.Graph.RootsPath.IsEmpty then
                                                    yield button [Button.Color IsPrimary; Button.Size IsSmall] "Back" (fun _ -> dispatch (Graph <| GraphMsg.RootBack))]
                                              Column.column [ Column.Width(Screen.All, Column.Is3) ]
                                                [button [Button.Color IsDark; Button.Size IsSmall] "Free view" (fun _ -> dispatch (Graph <| GraphMsg.SetOptions {model.Graph.Options with Layout = Graph.Free}))]
                                              Column.column [ Column.Width(Screen.All, Column.Is3) ]
                                                [button [Button.Color IsDark; Button.Size IsSmall] "Left-Right view" (fun _ -> dispatch (Graph <| GraphMsg.SetOptions {model.Graph.Options with Layout = Graph.HierarchicalLeftRight}))]
                                              Column.column [ Column.Width(Screen.All, Column.Is3) ]
                                                [button [Button.Color IsDark; Button.Size IsSmall] "Upper-Down view" (fun _ -> dispatch (Graph <| GraphMsg.SetOptions {model.Graph.Options with Layout = Graph.HierarchicalUpDown}))]
                                            ]
                                    div [] [
                                        graphButtons
                                        GraphView.graph [
                                            GraphView.Props.Tree (buildTree root |> limitTreeByNodes model.Graph.Options.MaxNodes)
                                            GraphView.Props.OnHover onHover
                                            GraphView.Props.OnSelect onSelect
                                            GraphView.Props.Options model.Graph.Options
                                            ] ]
                            | Result.Error errors ->
                                Editor.editor [ Editor.Language "fsharp"
                                                Editor.IsReadOnly true
                                                Editor.Value errors ]
                            | Ok None -> str ""

                     ] ]
            ]

    [ headers ; editors ]

let buttonsView sourceTooBig (model : Model) (dispatch : Msg -> unit)=
    Columns.columns []
                    [ Column.column [ Column.Width(Screen.All, Column.IsHalf) ]
                        [button sourceTooBig (if model.IsLoading then "Working..." else "Show Untyped AST") (fun _ -> dispatch (DoParse))]
                      Column.column [ Column.Width(Screen.All, Column.IsHalf) ]
                        [button sourceTooBig (if model.IsLoading then "Working..." else "Show Typed AST") (fun _ -> dispatch (DoTypeCheck))]
                    ]

let graphOptionsView (model : Model) (dispatch : Msg -> unit) =
    let input label value update =
        Field.div [ ] [ Label.label [ ] [ str label ]; Control.div [] [ Input.number [
        Input.Option.Value (string value); Input.Option.OnChange (fun ev ->
            dispatch (Graph (GraphMsg.SetOptions <| update !!ev.target?value)))] ] ]

    Columns.columns [Columns.Option.Props [Id "options"]] [
        Column.column [] []
        Column.column [] [
            strong [] [str "Graph options:"]
            input "MaxNodes" model.Graph.Options.MaxNodes (fun x -> {model.Graph.Options with MaxNodes = x})
            input "MaxNodesInRow" model.Graph.Options.MaxNodesInRow (fun x -> {model.Graph.Options with MaxNodesInRow = x})
    ] ]

let footer =
    Footer.footer [ ]
                  [ Content.content [ Content.Modifiers [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ] ] safeComponents ]

let view (model : Model) (dispatch : Msg -> unit) =
    let sourceTooBig = model.Source.Length > Const.sourceSizeLimit

    div []
        [ viewNavbar model.Version
          div [ Class "page-content"]
                  [
                    yield! sourceAndFormatted model dispatch sourceTooBig
                    yield buttonsView sourceTooBig model dispatch
                    if model.View = Shared.Graph then yield graphOptionsView model dispatch
                    yield footer
                  ]
        ]


#if DEBUG
open Elmish.Debug
open Elmish.HMR
#endif

Program.mkProgram init update view
#if DEBUG
|> Program.withConsoleTrace
|> Program.withHMR
#endif
|> Program.withReact "elmish-app"
#if DEBUG
|> Program.withDebugger
#endif
|> Program.run
